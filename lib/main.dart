import 'package:flutter/material.dart';
// import 'package:hive/hive.dart';
// import 'package:path_provider/path_provider.dart';
// import 'package:project_fix/hive_db/data_model.dart';
// import 'package:project_fix/hive_db/my_home_page.dart';
import 'screens/login_page.dart';

void main() {
  // WidgetsFlutterBinding.ensureInitialized();
  // final document = await getApplicationDocumentsDirectory();
  // Hive.init(document.path);
  // Hive.registerAdapter(DataModelAdapter());
  // await Hive.openBox<DataModel>(dataBoxName);
  // runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Authentication',
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      home: LoginPage(),
    );
  }
}
